#include "RigidBodySystemSimulator.h"

// Enum describing the test cases
enum Cases {
	DEMO1,
	DEMO2,
	DEMO3,
	DEMO4
};

RigidBodySystemSimulator::RigidBodySystemSimulator()
	: d_inputScale(0.02), 
	d_collisionBouncyness(1), 
	d_gravityIsEnabled(false), 
	d_gravity(Vec3(0, -9.81, 0))
{
}
	
const char * RigidBodySystemSimulator::getTestCasesStr()
{
	return "DEMO1, DEMO2, DEMO3, DEMO4";
}

void RigidBodySystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC = DUC;

	TwAddVarRW(DUC->g_pTweakBar, "Input Factor", TW_TYPE_FLOAT, &d_inputScale, "min=0 step=0.01");
}

void RigidBodySystemSimulator::reset()
{
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;

	d_rigidBodyList.clear();

	d_gravityIsEnabled = false;
}

void RigidBodySystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	DUC->setUpLighting(Vec3(0,0,0),0.4*Vec3(1,1,1),2000.0, Vec3(0.5,0.5,0.5));

	for (RigidBody const &body : d_rigidBodyList)
		DUC->drawRigidBody(body.obj2WorldMatrix());
		//DUC->drawSphere(body.position(), body.size());

}

void RigidBodySystemSimulator::setupDefaultSituation()
{
	// Add the rectangle with the correct size
	addRigidBody(Vec3(0.0, 0.0, 0.0), Vec3(1, 0.6, 0.5), 2);
	// Rotate the rectangle by 90 degrees in the Z axis
	setOrientationOf(0, Quat(Vec3(0, 0, 1), (float) M_PI / 2));
	// Apply the given force
	applyForceOnBody(0, Vec3(0.3, 0.5, 0.25), Vec3(1, 1, 0));
}

void RigidBodySystemSimulator::setupTwoBodiesSimulation() {
	addRigidBody(Vec3(-0.1f, -0.2f, 0.1f), Vec3(0.4f, 0.2f, 0.2f), 100);
	addRigidBody(Vec3(0.0f, 0.2f, 0.0f), Vec3(0.4f, 0.2f, 0.2f), 100);
	setOrientationOf(1, Quat(Vec3(0.0f, 0.0f, 1.0f), (float)(M_PI)*0.25f));
	setVelocityOf(1, Vec3(0.0f, -0.1f, 0.05f));
}

void RigidBodySystemSimulator::setupTwoBodiesSimulation2() {
	addRigidBody(Vec3(-0.3f, -0.4f, 0.0f), Vec3(0.8f, 0.05f, 0.2f), 100);
	addRigidBody(Vec3(0.3f, 0.1f, 0.0f), Vec3(0.4f, 0.4f, 0.4f), 800);
	setOrientationOf(0, Quat(Vec3(0.0f, 0.0f, 1.0f), (float)(M_PI)*0.5f));
	setOrientationOf(1, Quat(Vec3(0.0f, 0.0f, 1.0f), (float)(M_PI)*0.5f));
	setVelocityOf(0, Vec3(0.1f, 0.0f, 0.0f));
	setVelocityOf(1, Vec3(-0.1f, 0.0f, 0.0f));
}

void RigidBodySystemSimulator::setupComplexSimulation()
{
	addRigidBody(Vec3(0.75f, 0.0f, 0.0f), Vec3(0.1f), 100);
	setVelocityOf(0, Vec3(-0.1f, 0.0f, 0.0f));

	addRigidBody(Vec3(0.5f, 0.25f, 0.0f), Vec3(0.1f), 200);
	setVelocityOf(1, Vec3(0.0f, -0.15f, 0.0f));

	addRigidBody(Vec3(1.0f, -0.25f, 0.0f), Vec3(0.1f), 100);
	setVelocityOf(2, Vec3(-0.20f, -0.0f, 0.0f));
	
	addRigidBody(Vec3(1.5f, -1.5f, 0.0f), Vec3(0.1f), 100);
	setVelocityOf(3, Vec3(-0.40f, 0.4f, 0.0f));
	
	addRigidBody(Vec3(-1.5, -2.5f, 0.0f), Vec3(0.1f), 100);
	setVelocityOf(4, Vec3(0.20f, 0.4f, 0.0f));
	
	addRigidBody(Vec3(-3.5, 0.4f, 0.0f), Vec3(0.1f), 100);
	setVelocityOf(5, Vec3(0.50f, -0.1f, 0.0f));
	
	addRigidBody(Vec3(-2.5, 2.4f, 0.0f), Vec3(0.1f), 100);
	setVelocityOf(6, Vec3(0.30f, -0.4f, 0.0f));
	
	addRigidBody(Vec3(0.0f, -9.5f, 0.0f), Vec3(0.1f), 100);
	setVelocityOf(7, Vec3(-0.1f, 1.4f, 0.0f));


	int boxesPerRow = 3;
	float boxSize = 0.3f / boxesPerRow;
	float startY = -0.25f;
	float startX = -0.25f;
	for (float y = startY; y <= startY + boxSize * boxesPerRow; y += boxSize) {
		for (float x = startX; x <= startX + boxSize * boxesPerRow; x += boxSize) {
			addRigidBody(Vec3(x, y, 0.0f), Vec3(boxSize), 100.0);
		}
	}
}

void RigidBodySystemSimulator::setupComplexSimulation2()
{
	int numberOfTurns = 5;
	float boxSize = 0.02f;
	int numberOfBoxes = 128;
	float startRadius = 0.6;
	float endRadius = 0.15;
	float radiusStep = (startRadius - endRadius) / numberOfBoxes;
	float radius = startRadius;
	float sinCosStep = 2 * M_PI / numberOfBoxes * numberOfTurns;
	float curSinCos = 0;
	float speed = 0.15;
	for (int i = 0; i < numberOfBoxes; i++) {
		Vec3 position = Vec3(sin(curSinCos), cos(curSinCos), 0);
		Vec3 velocity = -position;
		position *= radius;
		velocity *= speed * radius / 2;
		addRigidBody(position, boxSize * (radius / startRadius) * 2, 100);
		setVelocityOf(i, velocity);
		setOrientationOf(i, Quat(Vec3(0, 0, 1), curSinCos / 2));
		radius -= radiusStep;
		curSinCos += sinCosStep;
	}

	addRigidBody(Vec3(0, 0, 5), 0.1, 2000);
	setVelocityOf(numberOfBoxes, Vec3(0, 0, -0.5));
}

void RigidBodySystemSimulator::printRigidProperties(RigidBody const &body)
{
	cout << "Velocity: " << body.velocity()
		<< ", Angular Velocity: " << body.angleVelocity() << "\n"; 
}

void RigidBodySystemSimulator::handleCollisions()
{
	for (int i = 0; i < d_rigidBodyList.size(); ++i) {
		RigidBody &bodyA = d_rigidBodyList[i];

		for (int j = i + 1; j < d_rigidBodyList.size(); ++j) {
			RigidBody &bodyB = d_rigidBodyList[j];

			// Doing a simpler collision check before SAT
			if (sqrt(bodyA.position().squaredDistanceTo(bodyB.position())) > 
				(
					sqrt(bodyA.size().squaredDistanceTo(Vec3(0.0))) / 2 + 
					sqrt(bodyB.size().squaredDistanceTo(Vec3(0.0))) / 2
				))
				continue;

			CollisionInfo colInfo = checkCollisionSAT(bodyA.obj2WorldMatrix(),	bodyB.obj2WorldMatrix());

			// If collision occured
			if (colInfo.isValid) {

				// Calculate relative collision positions
				Vec3 collisionPointA = colInfo.collisionPointWorld - bodyA.position();
				Vec3 collisionPointB = colInfo.collisionPointWorld - bodyB.position();

				// Calculate velocities of collision points on each body
				Vec3 velPointA = bodyA.velocity() + cross(bodyA.angleVelocity(), collisionPointA);
				Vec3 velPointB = bodyB.velocity() + cross(bodyB.angleVelocity(), collisionPointB);

				// Relative velocity between the two bodies collision points
				Vec3 relVelocity = velPointA - velPointB;

				// Bodies are moving into each other (colliding) only if this is true
				if (dot(relVelocity, colInfo.normalWorld) < 0) {
					float inverseMassA = bodyA.fixed() ? 0 : 1.0f / bodyA.mass();
					float inverseMassB = bodyB.fixed() ? 0 : 1.0f / bodyB.mass();

					float J = (-(1 + d_collisionBouncyness) * dot(relVelocity, colInfo.normalWorld)) /
						(inverseMassA + 
							inverseMassB + 
							dot(
								cross(bodyA.inTensInv().transformVector(cross(collisionPointA, colInfo.normalWorld)), collisionPointA) + 
								cross(bodyB.inTensInv().transformVector(cross(collisionPointB, colInfo.normalWorld)), collisionPointB), 
								colInfo.normalWorld
							)
						);

					bodyA.velocity(bodyA.velocity() + J * colInfo.normalWorld / bodyA.mass());
					bodyB.velocity(bodyB.velocity() - J * colInfo.normalWorld / bodyB.mass());
					bodyA.angularMom(bodyA.angularMom() + cross(collisionPointA, J * colInfo.normalWorld));
					bodyB.angularMom(bodyB.angularMom() - cross(collisionPointB, J * colInfo.normalWorld));
				}
			}
		}
	}
}

void RigidBodySystemSimulator::notifyCaseChanged(int testCase)
{
	reset();
	m_iTestCase = testCase;

	switch (m_iTestCase)
	{
	case DEMO1:
	{
		setupDefaultSituation();
		cout << "Demo 1, one timesteps (h=2) for the rigid body simulation\n";
		simulateTimestep(2);
		printRigidProperties(d_rigidBodyList[0]);
		break;
	}
	case DEMO2:
	{
		simulateTimestep(0.01);
		setupDefaultSituation();
		break;
	}
	case DEMO3:
	{
		setupTwoBodiesSimulation2();
		break;
	}
	case DEMO4:
	{
		setupComplexSimulation2();
		break;
	}
	}
}

void RigidBodySystemSimulator::externalForcesCalculations(float timeElapsed)
{
	Point2D mouseDiff;
	mouseDiff.x = m_trackmouse.x - m_oldtrackmouse.x;
	mouseDiff.y = m_trackmouse.y - m_oldtrackmouse.y;
	
	if (mouseDiff.x != 0 || mouseDiff.y != 0) {
		Vec3 mouseForce = Vec3(mouseDiff.x, -mouseDiff.y, 0) * d_inputScale;
		for (RigidBody &body : d_rigidBodyList) {
			if (body.fixed())
				continue;

			body.applyForce(body.position(), mouseForce);
		}
	}

	if (d_gravityIsEnabled) {
		for (RigidBody &body : d_rigidBodyList) {
			if (body.fixed())
				continue;

			body.applyForce(body.position(), d_gravity * body.mass());
		}
	}
}

void RigidBodySystemSimulator::simulateTimestep(float timeStep)
{
	handleCollisions();

	for (RigidBody &body : d_rigidBodyList)
	{
		if (body.fixed())
			continue;
		// Update position
		body.position(body.position() + timeStep * body.velocity());
		// Update velocity
		body.velocity(body.velocity() + timeStep * body.force() / body.mass());
		// Update orientation quaterion
		body.orientation(body.orientation() +
			(timeStep / 2) * body.angleVelocityQuat() * body.orientation());
		// Update angular momentum
		body.angularMom(body.angularMom() + timeStep * body.torque());
		body.angleVelocity(body.inTensInv() * body.angularMom());

		body.resetForce();
	}
	
}

void RigidBodySystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}

void RigidBodySystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}

void RigidBodySystemSimulator::onKey(int key)
{
	//cout << "key nr: " << key << "\n";
	// H = 72
	if (key == 72)
		d_rigidBodyList[0].torque(
			d_rigidBodyList[0].torque() +
			Vec3(0, -1, 0));
	// J == 74
	if (key == 74)
		d_rigidBodyList[0].torque(
			d_rigidBodyList[0].torque() +
			Vec3(0, 0, 1));
	// K == 75
	if (key == 75)
		d_rigidBodyList[0].torque(
			d_rigidBodyList[0].torque() +
			Vec3(0, 0, -1));
	// L == 76
	if (key == 76)
		d_rigidBodyList[0].torque(
			d_rigidBodyList[0].torque() +
			Vec3(0, 1, 0));
}

// ExtraFunctions
int RigidBodySystemSimulator::getNumberOfRigidBodies()
{
	return d_rigidBodyList.size();
}

Vec3 RigidBodySystemSimulator::getPositionOfRigidBody(int i)
{
	return d_rigidBodyList[i].position();
}

Vec3 RigidBodySystemSimulator::getLinearVelocityOfRigidBody(int i)
{
	return d_rigidBodyList[i].velocity();
}

Vec3 RigidBodySystemSimulator::getAngularVelocityOfRigidBody(int i)
{
	return d_rigidBodyList[i].angleVelocity();
}

void RigidBodySystemSimulator::applyForceOnBody(int i, Vec3 loc, Vec3 force)
{
	d_rigidBodyList[i].applyForce(loc, force);
}

void RigidBodySystemSimulator::addRigidBody(Vec3 position, Vec3 size, int mass, bool fixed)
{
	d_rigidBodyList.push_back(RigidBody(position, size, mass, fixed));
}

void RigidBodySystemSimulator::setOrientationOf(int i, Quat orientation)
{
	d_rigidBodyList[i].orientation(orientation);
}

void RigidBodySystemSimulator::setVelocityOf(int i, Vec3 velocity) 
{
	d_rigidBodyList[i].velocity(velocity);
}
