#ifndef RIGIDBODYSYSTEMSIMULATOR_h
#define RIGIDBODYSYSTEMSIMULATOR_h
#include "Simulator.h"
#include "RigidBody.h"
#include "collisionDetect.h"

#define TESTCASEUSEDTORUNTEST 2

class RigidBodySystemSimulator:public Simulator
{
	bool d_applyMouseForce;
	float d_inputScale;
	// 1 = elastic, 0 = plastic
	float d_collisionBouncyness;
	bool d_gravityIsEnabled;
	Vec3 d_gravity;

	std::vector<RigidBody> d_rigidBodyList;

	// Attributes
	// add your RigidBodySystem data members, for e.g.,
	// RigidBodySystem * m_pRigidBodySystem; 
	Vec3 m_externalForce;

	// UI Attributes
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;

public:
	// Construtors
	RigidBodySystemSimulator();
	
	// Functions
	const char * getTestCasesStr();
	void initUI(DrawingUtilitiesClass * DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void externalForcesCalculations(float timeElapsed);
	void simulateTimestep(float timeStep);
	void onClick(int x, int y);
	void onMouse(int x, int y);
	void onKey(int key);

	// ExtraFunctions
	int getNumberOfRigidBodies();
	Vec3 getPositionOfRigidBody(int i);
	Vec3 getLinearVelocityOfRigidBody(int i);
	Vec3 getAngularVelocityOfRigidBody(int i);
	void applyForceOnBody(int i, Vec3 loc, Vec3 force);
	void addRigidBody(Vec3 position, Vec3 size, int mass, bool fixed = false);
	void setOrientationOf(int i,Quat orientation);
	void setVelocityOf(int i, Vec3 velocity);

private:

	void setupDefaultSituation();
	void setupTwoBodiesSimulation();
	void setupTwoBodiesSimulation2();
	void setupComplexSimulation();
	void setupComplexSimulation2();
	void printRigidProperties(RigidBody const &body);
	void handleCollisions();
	
};
#endif