#ifndef MASSPOINT_H
#define MASSPOINT_H

#include "util/vectorbase.h"

class MassPoint
{
	// Data	
	GamePhysics::Vec3 d_position;
	GamePhysics::Vec3 d_velocity;
	GamePhysics::Vec3 d_force;
	float d_mass;
	float d_damping;

	bool d_isFixed;
	// If false the mass point is not influenced by user interaction
	bool d_userInteraction;

	GamePhysics::Vec3 d_x_tmp;
	GamePhysics::Vec3 d_v_tmp;

public:
	MassPoint(GamePhysics::Vec3 position, GamePhysics::Vec3 velocity, bool isFixed, bool userInteraction = true);

	void resetForce();
	void addForce(GamePhysics::Vec3 amount);
	void updateBoundingBox();

	// Getters
	GamePhysics::Vec3 position() const;
	GamePhysics::Vec3 velocity() const;
	GamePhysics::Vec3 force() const;
	
	bool isFixed() const;
	bool userInteraction() const;

	GamePhysics::Vec3 x_tmp() const;
	GamePhysics::Vec3 v_tmp() const;

	// Setters
	void position(GamePhysics::Vec3 position);
	void velocity(GamePhysics::Vec3 velocity);

	void x_tmp(GamePhysics::Vec3 x_tmp);
	void v_tmp(GamePhysics::Vec3 v_tmp);

private:
	bool updateBoundingBoxPosition(GamePhysics::Real *pos);
};

inline void MassPoint::resetForce()
{
	d_force = 0;
}

inline void MassPoint::addForce(GamePhysics::Vec3 amount)
{
	d_force += amount;
}

inline GamePhysics::Vec3 MassPoint::position() const
{
	return d_position;
}

inline GamePhysics::Vec3 MassPoint::velocity() const
{
	return d_velocity;
}

inline GamePhysics::Vec3 MassPoint::force() const
{
	return d_force;
}

inline bool MassPoint::isFixed() const
{
	return d_isFixed;
}

inline bool MassPoint::userInteraction() const
{
	return d_userInteraction;
}

inline GamePhysics::Vec3 MassPoint::x_tmp() const
{
	return d_x_tmp;
}

inline GamePhysics::Vec3 MassPoint::v_tmp() const
{
	return d_v_tmp;
}

inline void MassPoint::position(GamePhysics::Vec3 position)
{
	if (!d_isFixed)
		d_position = position;
}

inline void MassPoint::velocity(GamePhysics::Vec3 velocity)
{
	d_velocity = velocity;
}

inline void MassPoint::x_tmp(GamePhysics::Vec3 x_tmp)
{
	d_x_tmp = x_tmp;
}

inline void MassPoint::v_tmp(GamePhysics::Vec3 v_tmp)
{
	d_v_tmp = v_tmp;
}

#endif // !MASSPOINT_H
