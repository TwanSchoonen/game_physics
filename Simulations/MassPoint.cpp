#include "MassPoint.h"

using namespace GamePhysics;

MassPoint::MassPoint(Vec3 position, Vec3 velocity, bool isFixed, bool userInteraction)
	: d_position(position),
	d_velocity(velocity),
	d_isFixed(isFixed),
	d_userInteraction(userInteraction)
{
}

bool MassPoint::updateBoundingBoxPosition(Real *pos)
{
	if (*pos < -0.5 || *pos > 0.5)
	{
		if (*pos < -0.5)
			*pos = -0.5;
		else
			*pos = 0.5;
		return true;
	}
	return false;
}

void MassPoint::updateBoundingBox()
{
	if (updateBoundingBoxPosition(&d_position.x))
		d_velocity.x = 0;

	if (updateBoundingBoxPosition(&d_position.y))
		d_velocity.y = 0;

	if (updateBoundingBoxPosition(&d_position.z))
		d_velocity.z = 0;
}

