#ifndef MASSSPRINGSYSTEMSIMULATOR_h
#define MASSSPRINGSYSTEMSIMULATOR_h
#include "Simulator.h"
#include "MassPoint.h"
#include "Spring.h"

// Do Not Change
#define EULER 0
#define LEAPFROG 1
#define MIDPOINT 2
// Do Not Change


class MassSpringSystemSimulator:public Simulator
{
	// Forces
	float m_fMass;
	float m_fStiffness;
	float m_fDamping;
	float m_fGravity;

	// Data Attributes
	float d_sphereSize;
	float d_inputScale;

	int m_iIntegrator;

	std::vector<MassPoint> d_massPointList;
	std::vector<Spring> d_springList;

	// UI Attributes
	Vec3 m_externalForce;
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;

public:
	// Construtors
	MassSpringSystemSimulator();
	
	// UI Functions
	const char * getTestCasesStr();
	void initUI(DrawingUtilitiesClass * DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void externalForcesCalculations(float timeElapsed);

	void simulateTimestep(float timeStep);
	void onClick(int x, int y);
	void onMouse(int x, int y);

	// Specific Functions
	void setMass(float mass);
	void setStiffness(float stiffness);
	void setDampingFactor(float damping);
	void setGravity(float gravity);
	int addMassPoint(Vec3 position, Vec3 velocity, bool isFixed, bool userInteraction = true);
	void addSpring(int masspoint1, int masspoint2, float initialLength);
	int getNumberOfMassPoints();
	int getNumberOfSprings();
	Vec3 getPositionOfMassPoint(int index);
	Vec3 getVelocityOfMassPoint(int index);
	void applyExternalForce(Vec3 force);
	
	// Do Not Change
	void setIntegrator(int integrator) {
		m_iIntegrator = integrator;
	}

private:
	void setupDefaultSituation();
	void setupComplexSituation();
	void setupAnotherComplexSituation();
	void setupLeapFrogSituation();
	void simulateTimestepEuler(float timeStep);
	void simulateTimestepLeapFrog(float timeStep);
	void simulateTimestepMidpoint(float timeStep);
	void addInternalForces();
};
#endif