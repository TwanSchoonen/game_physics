#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include "util/matrixbase.h"
#include "util/quaternion.h"
#include "util/vectorbase.h"

class RigidBody
{
	GamePhysics::Vec3 d_position;
	GamePhysics::Vec3 d_velocity;
	GamePhysics::Vec3 d_force;

	int d_mass;
	GamePhysics::Vec3 d_size;

	GamePhysics::Vec3 d_torque;
	GamePhysics::Vec3 d_angleVelocity;
	GamePhysics::Vec3 d_angularMom;

	GamePhysics::Quat d_orientation;
	GamePhysics::Mat4 d_inTensInv;

	bool d_fixed;

public:
	RigidBody(GamePhysics::Vec3 position, GamePhysics::Vec3 size,
		int mass, bool fixed = false);

	void initializeInertiaTensor();
	void resetForce();
	void applyForce(GamePhysics::Vec3 loc, GamePhysics::Vec3 force);

	GamePhysics::Mat4 scaleMat() const;
	GamePhysics::Mat4 rotMat() const;
	GamePhysics::Mat4 transMat() const;
	GamePhysics::Mat4 obj2WorldMatrix() const;
	GamePhysics::Mat4 inTensInv() const;

	// Getters - Setters
	GamePhysics::Vec3 position() const;
	GamePhysics::Vec3 velocity() const;
	GamePhysics::Vec3 force() const;
	int mass() const;
	GamePhysics::Vec3 size() const;

	GamePhysics::Vec3 torque() const;
	GamePhysics::Vec3 angleVelocity() const;
	GamePhysics::Vec3 angularMom() const;
	GamePhysics::Quat angleVelocityQuat() const;
	GamePhysics::Quat orientation() const;

	bool fixed() const;

	void position(GamePhysics::Vec3 position);
	void velocity(GamePhysics::Vec3 velocity);
	void torque(GamePhysics::Vec3 torque);
	void angleVelocity(GamePhysics::Vec3 angleVelocity);
	void angularMom(GamePhysics::Vec3 angularMom);
	void orientation(GamePhysics::Quat orientation);
};

inline GamePhysics::Vec3 RigidBody::position() const
{
	return d_position;
}

inline GamePhysics::Vec3 RigidBody::velocity() const
{
	return d_velocity;
}

inline GamePhysics::Vec3 RigidBody::force() const
{
	return d_force;
}

inline int RigidBody::mass() const
{
	return d_mass;
}

inline GamePhysics::Vec3 RigidBody::size() const
{
	return d_size;
}

inline GamePhysics::Vec3 RigidBody::torque() const
{
	return d_torque;
}

inline GamePhysics::Vec3 RigidBody::angleVelocity() const
{
	return d_angleVelocity;
}

inline GamePhysics::Vec3 RigidBody::angularMom() const
{
	return d_angularMom;
}

inline GamePhysics::Quat RigidBody::angleVelocityQuat() const
{
	return GamePhysics::Quat(d_angleVelocity.x, d_angleVelocity.y,
		d_angleVelocity.z, 0);
}

inline GamePhysics::Quat RigidBody::orientation() const
{
	return d_orientation;
}

inline bool RigidBody::fixed() const
{
	return d_fixed;
}

inline void RigidBody::position(GamePhysics::Vec3 position)
{
	d_position = position;
}

inline void RigidBody::velocity(GamePhysics::Vec3 velocity)
{
	d_velocity = velocity;
}

inline void RigidBody::torque(GamePhysics::Vec3 torque)
{
	d_torque = torque;
}

inline void RigidBody::angleVelocity(GamePhysics::Vec3 angleVelocity)
{
	d_angleVelocity = angleVelocity;
}

inline void RigidBody::angularMom(GamePhysics::Vec3 angularMom)
{
	d_angularMom = angularMom;
}

inline void RigidBody::orientation(GamePhysics::Quat orientation)
{
	d_orientation = orientation.unit();
}

#endif
