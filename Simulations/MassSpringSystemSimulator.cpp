#include "MassSpringSystemSimulator.h"
#include <cmath>

// Enum describing the test cases
enum Cases {
	DEMO1,
	DEMO2,
	DEMO3,
	DEMO4,
	DEMO5,
	DEMO6
};

MassSpringSystemSimulator::MassSpringSystemSimulator()
	: d_sphereSize(0.02),
	d_inputScale(0.2),
	m_iIntegrator(EULER)
{
}

const char *MassSpringSystemSimulator::getTestCasesStr()
{
	return "DEMO1, DEMO2, DEMO3, DEMO4, DEMO5, DEMO6";
}

void MassSpringSystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC = DUC;

	TwAddVarRW(DUC->g_pTweakBar, "Sphere Size", TW_TYPE_FLOAT, &d_sphereSize, "min=0.01 step=0.01");

	switch (m_iTestCase)
	{
	case DEMO1:
	case DEMO2:	
	case DEMO3:	
	{
		break;
	}
	case DEMO4:
	case DEMO6:
	{
		TwAddVarRW(DUC->g_pTweakBar, "Integrator", TW_TYPE_INT32, &m_iIntegrator, "min=0 step=2 max=2");
	}
	case DEMO5:
	{
		TwAddVarRW(DUC->g_pTweakBar, "Damping", TW_TYPE_FLOAT, &m_fDamping, "min=0 step=0.1");
		TwAddVarRW(DUC->g_pTweakBar, "Gravity", TW_TYPE_FLOAT, &m_fGravity, "min=0 step=0.1");
		TwAddVarRW(DUC->g_pTweakBar, "Stiffness", TW_TYPE_FLOAT, &m_fStiffness, "min=0 step=0.1");
		TwAddVarRW(DUC->g_pTweakBar, "Mass", TW_TYPE_FLOAT, &m_fMass, "min=0 step=0.1");
		break;
	}
	}
}

void MassSpringSystemSimulator::reset()
{
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;

	d_massPointList.clear();
	d_springList.clear();
}

void MassSpringSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	// do not draw DEMO1
	if (m_iTestCase == DEMO1)
		return;
	
	// Draw the spheres
	for (MassPoint const &massPoint : d_massPointList)
		DUC->drawSphere(massPoint.position(), d_sphereSize);

	// Draw all strings
	for (Spring const &spring : d_springList)
	{
		DUC->beginLine();
		DUC->drawLine(getPositionOfMassPoint(spring.massPoint1()), Vec3(1, 0, 0), 
			getPositionOfMassPoint(spring.massPoint2()), Vec3(1, 0, 0));
		DUC->endLine();
	}
}

// Sets the default parameters given in the exercise sheet
void MassSpringSystemSimulator::setupDefaultSituation()
{
	setMass(10);
	setStiffness(40);
	setDampingFactor(0);
	setGravity(0);

	addSpring(addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false),
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false), 1);
}

void MassSpringSystemSimulator::setupAnotherComplexSituation()
{
	setMass(1);
	setStiffness(2000);
	setDampingFactor(0.9);
	setGravity(5);

	double springLength = 0.05;
	double diagonalLength = springLength * sqrt(2);
	bool pointsFixed = false;

	int rows = 20;
    int cols = 20;

	int massPoints[20][20];
	for (int row = 0; row != rows; ++row)
	{
		for (int col = 0; col != cols; ++col)
		{
			if (row == 0 && (col == 0 || col == cols - 1))
				massPoints[row][col] = addMassPoint(Vec3(row * springLength - 0.5, 0.5, col * springLength - 0.5), Vec3(0, 0, 0),
					true);
			else
				massPoints[row][col] = addMassPoint(Vec3(row * springLength - 0.5, 0.5, col * springLength - 0.5), Vec3(0, 0, 0),
					pointsFixed);
		}
	}

	for (int row = 0; row != rows; ++row)
	{
		for (int col = 0; col != cols; ++col)
		{
			if (row > 0)
			{
				addSpring(massPoints[row][col], massPoints[row - 1][col], springLength);

				if (col < cols - 1)
					addSpring(massPoints[row][col], massPoints[row - 1][col + 1], diagonalLength);
			}

			if (col < cols - 1)
				addSpring(massPoints[row][col], massPoints[row][col + 1], springLength);
		}
	}
}

void MassSpringSystemSimulator::setupComplexSituation()
{
	setMass(10);
	setStiffness(400);
	setDampingFactor(0.9);
	setGravity(9.81);
	double springLength = 0.45;
	double edgeLenght = 0.5;
	double goldenRatio = edgeLenght * ((1 + sqrt(5)) / 2);
	double midLength = sqrt(pow(edgeLenght / 2, 2) + pow(goldenRatio / 2, 2));
	bool pointsFixed = false;
		
	int m1 = addMassPoint(Vec3(-edgeLenght / 2, goldenRatio / 2, 0), Vec3(0, 0, 0), pointsFixed, false);
	int m2 = addMassPoint(Vec3(edgeLenght / 2, goldenRatio / 2, 0), Vec3(0, 0, 0), pointsFixed, false);
	int m3 = addMassPoint(Vec3(edgeLenght / 2, -goldenRatio / 2, 0), Vec3(0, 0, 0), pointsFixed, false);
	int m4 = addMassPoint(Vec3(-edgeLenght / 2, -goldenRatio / 2, 0), Vec3(0, 0, 0), pointsFixed, false);
	int m5 = addMassPoint(Vec3(-goldenRatio / 2, 0, edgeLenght / 2), Vec3(0, 0, 0), pointsFixed, false);
	int m6 = addMassPoint(Vec3(goldenRatio / 2, 0, edgeLenght / 2), Vec3(0, 0, 0), pointsFixed, false);
	int m7 = addMassPoint(Vec3(goldenRatio / 2, 0, -edgeLenght / 2), Vec3(0, 0, 0), pointsFixed, false);
	int m8 = addMassPoint(Vec3(-goldenRatio / 2, 0, -edgeLenght / 2), Vec3(0, 0, 0), pointsFixed, false);
	int m9 = addMassPoint(Vec3(0, edgeLenght / 2, goldenRatio / 2), Vec3(0, 0, 0), pointsFixed, false);
	int m10 = addMassPoint(Vec3(0, edgeLenght / 2, -goldenRatio / 2), Vec3(0, 0, 0), pointsFixed, false);
	int m11 = addMassPoint(Vec3(0, -edgeLenght / 2, -goldenRatio / 2), Vec3(0, 0, 0), pointsFixed, false);
	int m12 = addMassPoint(Vec3(0, -edgeLenght / 2, goldenRatio / 2), Vec3(0, 0, 0), pointsFixed, false);
	int m13 = addMassPoint(Vec3(), Vec3(0, 0, 0), false);

	addSpring(m1, m2, springLength);
	addSpring(m1, m5, springLength);
	addSpring(m1, m8, springLength);
	addSpring(m1, m9, springLength);
	addSpring(m1, m10, springLength);

	addSpring(m2, m6, springLength);
	addSpring(m2, m7, springLength);
	addSpring(m2, m9, springLength);
	addSpring(m2, m10, springLength);

	addSpring(m3, m4, springLength);
	addSpring(m3, m6, springLength);
	addSpring(m3, m7, springLength);
	addSpring(m3, m12, springLength);
	addSpring(m3, m11, springLength);

	addSpring(m4, m5, springLength);
	addSpring(m4, m8, springLength);
	addSpring(m4, m12, springLength);
	addSpring(m4, m11, springLength);

	addSpring(m5, m8, springLength);
	addSpring(m5, m9, springLength);
	addSpring(m5, m12, springLength);

	addSpring(m6, m7, springLength);
	addSpring(m6, m9, springLength);
	addSpring(m6, m12, springLength);

	addSpring(m7, m10, springLength);
	addSpring(m7, m11, springLength);

	addSpring(m8, m10, springLength);
	addSpring(m8, m11, springLength);

	addSpring(m9, m12, springLength);
	addSpring(m10, m11, springLength);

	addSpring(m13, m1, midLength);
	addSpring(m13, m2, midLength);
	addSpring(m13, m3, midLength);
	addSpring(m13, m4, midLength);
	addSpring(m13, m5, midLength);
	addSpring(m13, m6, midLength);
	addSpring(m13, m7, midLength);
	addSpring(m13, m8, midLength);
	addSpring(m13, m9, midLength);
	addSpring(m13, m10, midLength);
	addSpring(m13, m11, midLength);
	addSpring(m13, m12, midLength);
}

void MassSpringSystemSimulator::setupLeapFrogSituation()
{
	setMass(2);
	setStiffness(40);
	setDampingFactor(1.5);
	setGravity(5);
	int m1 = addMassPoint(Vec3(0, .5, 0), Vec3(), true);
	int m2 = addMassPoint(Vec3(), Vec3(), false);

	int m3 = addMassPoint(Vec3(-.1, -.2, .1), Vec3(), false);
	int m4 = addMassPoint(Vec3(.1, -.2, .1), Vec3(), false);
	int m5 = addMassPoint(Vec3(.1, -.2, -.1), Vec3(), false);
	int m6 = addMassPoint(Vec3(-.1, -.2, -.1), Vec3(), false);

	addSpring(m1, m2, 0.1);

	addSpring(m2, m3, 0.1);
	addSpring(m2, m4, 0.1);
	addSpring(m2, m5, 0.1);
	addSpring(m2, m6, 0.1);

	addSpring(m3, m4, 0.2);
	addSpring(m3, m5, 0.3);
	addSpring(m3, m6, 0.2);
	addSpring(m4, m5, 0.2);
	addSpring(m4, m6, 0.3);
	addSpring(m5, m6, 0.2);
}

void MassSpringSystemSimulator::notifyCaseChanged(int testCase)
{
	reset();
	m_iTestCase = testCase;
	
	switch (m_iTestCase)
	{
	case DEMO1:
	{
		// Euler calculation
		setupDefaultSituation();
		cout << "TestCase 1 calculate one step for Euler and midpoint\n";
		cout << "Euler:\n";
		addInternalForces();
		simulateTimestepEuler(0.1);
		for (size_t idx = 0; idx != d_massPointList.size(); ++idx)
		{
			cout << "MassPoint #" << idx << "\n";
			cout << "Position: " << d_massPointList[idx].position() << "\n";
			cout << "Velocity: " << d_massPointList[idx].velocity() << "\n";
		}
		reset();

		// Midpoint calculation
		cout << "\nMidpoint:\n";
		setupDefaultSituation();
		addInternalForces();
		simulateTimestepMidpoint(0.1);
		for (size_t idx = 0; idx != d_massPointList.size(); ++idx)
		{
			cout << "MassPoint #" << idx << "\n";
			cout << "Position: " << d_massPointList[idx].position() << "\n";
			cout << "Velocity: " << d_massPointList[idx].velocity() << "\n";
		}
		break;
	}
	case DEMO2:
	{
		setupDefaultSituation();
		setIntegrator(EULER);
		break;
	}
	case DEMO3:
	{
		setupDefaultSituation();
		setIntegrator(MIDPOINT);
		break;
	}
	case DEMO4:
	{
		setupComplexSituation();
		break;
	}
	case DEMO5:
	{
		setIntegrator(LEAPFROG);
		setupLeapFrogSituation();
		break;
	}
	case DEMO6:
	{
		setIntegrator(MIDPOINT);
		setupAnotherComplexSituation();
		break;
	}
	}
}

void MassSpringSystemSimulator::externalForcesCalculations(float timeElapsed)
{
	Point2D mouseDiff;
	mouseDiff.x = m_trackmouse.x - m_oldtrackmouse.x;
	mouseDiff.y = m_trackmouse.y - m_oldtrackmouse.y;
	if (mouseDiff.x != 0 || mouseDiff.y != 0)
		applyExternalForce(Vec3((float)mouseDiff.x, (float)-mouseDiff.y, 0) * d_inputScale);
	else
		applyExternalForce(Vec3());
}

void MassSpringSystemSimulator::simulateTimestepEuler(float timeStep)
{
	for (MassPoint &massPoint : d_massPointList)
	{
		massPoint.position(massPoint.position() + timeStep * massPoint.velocity());
		massPoint.velocity(massPoint.velocity() + timeStep * (massPoint.force() / m_fMass));
	}
}

void MassSpringSystemSimulator::simulateTimestepLeapFrog(float timeStep)
{
	for (MassPoint &massPoint : d_massPointList)
	{
		massPoint.velocity(massPoint.velocity() + timeStep * (massPoint.force() / m_fMass));
		massPoint.position(massPoint.position() + timeStep * massPoint.velocity());
	}
}

void MassSpringSystemSimulator::simulateTimestepMidpoint(float timeStep)
{
	for (MassPoint &massPoint : d_massPointList)
	{
		massPoint.x_tmp(massPoint.position() + (timeStep / 2) * massPoint.velocity());
		massPoint.v_tmp(massPoint.velocity() + (timeStep / 2) * (massPoint.force() / m_fMass));
		massPoint.position(massPoint.position() + timeStep * massPoint.v_tmp());
	}

	for (MassPoint &massPoint : d_massPointList)
	{
		massPoint.resetForce();
		massPoint.addForce(-Vec3(0, m_fGravity, 0));
		if (massPoint.userInteraction())
			massPoint.addForce(m_externalForce);
	}

	for (Spring &spring : d_springList)
	{
		MassPoint &m1 = d_massPointList[spring.massPoint1()];
		MassPoint &m2 = d_massPointList[spring.massPoint2()];
		// l = sqrt(x_i^2 + x_j^2)
		double currentLength = sqrt(m1.x_tmp().squaredDistanceTo(m2.x_tmp()));

		if (currentLength < 10e-4)
			currentLength = 10e-4;

		Vec3 springForce = -m_fStiffness * (currentLength - spring.initialLength())
			* ((m1.x_tmp() - m2.x_tmp()) / currentLength);
		// F_int_i_j = -k * (l-L) * (x_i - x_j) / l
		m1.addForce(springForce);
		// F_int_j_i = - F_int_i_j	
		m2.addForce(-springForce);
		// Add damping
		m1.addForce(-m_fDamping * m1.v_tmp());
		m2.addForce(-m_fDamping * m2.v_tmp());
	}

	for (MassPoint &massPoint : d_massPointList)
		massPoint.velocity(massPoint.velocity() + timeStep * (massPoint.force() / m_fMass));
}

void MassSpringSystemSimulator::addInternalForces()
{
	// For all mass points apply the spring force
	for (Spring &spring : d_springList)
	{
		MassPoint &m1 = d_massPointList[spring.massPoint1()];
		MassPoint &m2 = d_massPointList[spring.massPoint2()];
		// l = sqrt(x_i^2 + x_j^2)
		double currentLength = sqrt(m1.position().squaredDistanceTo(m2.position()));
		// F_int_i_j = -k * (l-L) * (x_i - x_j) / l
		// Make sure we do not divide by zero
		if (currentLength < 10e-4)
			currentLength = 10e-4;

		Vec3 springForce = -m_fStiffness * (currentLength - spring.initialLength())
			* ((m1.position() - m2.position()) / currentLength);
		m1.addForce(springForce);
		// F_int_j_i = - F_int_i_j	
		m2.addForce(-springForce);
		//add Damping
		m1.addForce(-m_fDamping * m1.velocity());
		m2.addForce(-m_fDamping * m2.velocity());
	}
}

void MassSpringSystemSimulator::simulateTimestep(float timeStep)
{
	// Dont update if we do DEMO1
	if (m_iTestCase == DEMO1)
		return;

	for (MassPoint &massPoint : d_massPointList)
	{
		massPoint.resetForce();
		massPoint.addForce(-Vec3(0, m_fGravity, 0));
		if (massPoint.userInteraction())
			massPoint.addForce(m_externalForce);
	}

	addInternalForces();

	switch (m_iIntegrator)
	{
	case EULER:
		simulateTimestepEuler(timeStep);
		break;
	case LEAPFROG:
		simulateTimestepLeapFrog(timeStep);
		break;
	case MIDPOINT:
		simulateTimestepMidpoint(timeStep);
		break;	
	}

	// Makes sure that the masspoint stay inside the bounding box
	// Only do this in the more complex situtations
	if (m_iTestCase == DEMO4 || m_iTestCase == DEMO5 || m_iTestCase == DEMO6)
		for (MassPoint &massPoint : d_massPointList)
			massPoint.updateBoundingBox();
}

void MassSpringSystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}

void MassSpringSystemSimulator::onMouse(int x, int y) 
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}


void MassSpringSystemSimulator::setMass(float mass)
{
	m_fMass = mass;
}

void MassSpringSystemSimulator::setStiffness(float stiffness)
{
	m_fStiffness = stiffness;
}

void MassSpringSystemSimulator::setDampingFactor(float damping)
{
	m_fDamping = damping;
}

void MassSpringSystemSimulator::setGravity(float gravity)
{
	m_fGravity = gravity;
}

int MassSpringSystemSimulator::addMassPoint(Vec3 position, Vec3 velocity, bool isFixed, bool userInteraction)
{
	d_massPointList.push_back(MassPoint(position, velocity, isFixed, userInteraction));
	return d_massPointList.size() - 1;
}

void MassSpringSystemSimulator::addSpring(int masspoint1, int masspoint2, float initialLength)
{
	d_springList.push_back(Spring(masspoint1, masspoint2, initialLength));
}

int MassSpringSystemSimulator::getNumberOfMassPoints()
{
	return d_massPointList.size();
}

int MassSpringSystemSimulator::getNumberOfSprings()
{
	return d_springList.size();
}

Vec3 MassSpringSystemSimulator::getPositionOfMassPoint(int index)
{
	return d_massPointList[index].position();
}

Vec3 MassSpringSystemSimulator::getVelocityOfMassPoint(int index)
{
	return d_massPointList[index].velocity();
}

void MassSpringSystemSimulator::applyExternalForce(Vec3 force)
{
	m_externalForce = force;
}
