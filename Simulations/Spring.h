#ifndef SPRING_H
#define SPRING_H

class Spring
{

	int d_massPoint1;
	int d_massPoint2;

	float d_initialLength;
	float d_stiffness;

public:
	Spring(int massPoint1, int massPoint2, float initialLenght);

	int massPoint1() const;
	int massPoint2() const;

	float initialLength() const;
};


inline int Spring::massPoint1() const
{
	return d_massPoint1;
}

inline int Spring::massPoint2() const
{
	return d_massPoint2;
}

inline float Spring::initialLength() const
{
	return d_initialLength;
}

#endif // !SPRING_H
