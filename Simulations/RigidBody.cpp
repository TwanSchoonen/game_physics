#include "RigidBody.h"

using namespace GamePhysics;

RigidBody::RigidBody(Vec3 position, Vec3 size,
	int mass, bool fixed)
	: d_position(position),
	d_size(size),
	d_mass(mass),
	d_fixed(fixed),
	d_orientation(0,0,0,1)
{
	initializeInertiaTensor();
}

void RigidBody::initializeInertiaTensor()
{
	d_inTensInv = Mat4(
		(1.0 / 12) * d_mass * (d_size.y * d_size.y + d_size.z * d_size.z), 0, 0, 0,
		0, (1.0 / 12) * d_mass * (d_size.x * d_size.x + d_size.z * d_size.z), 0, 0,
		0, 0, (1.0 / 12) * d_mass * (d_size.x * d_size.x + d_size.y * d_size.y), 0,
		0, 0, 0, 1
	).inverse();
}

void RigidBody::resetForce()
{
	d_force = Vec3(0, 0, 0);
	d_torque = Vec3(0, 0, 0);
}

void RigidBody::applyForce(Vec3 loc, Vec3 force)
{
	d_force += force;
	Vec3 relLoc = loc - d_position;
	d_torque += cross(relLoc, force);
}

Mat4 RigidBody::scaleMat() const
{
	Mat4 scaleMat;
	scaleMat.initScaling(d_size.x, d_size.y, d_size.z);
	return scaleMat;
}

Mat4 RigidBody::rotMat() const
{
	return d_orientation.getRotMat();
}

Mat4 RigidBody::transMat() const
{
	GamePhysics::Mat4 transMat;
	transMat.initTranslation(d_position.x, d_position.y, d_position.z);
	return transMat;
}

Mat4 RigidBody::obj2WorldMatrix() const
{
	return scaleMat() * rotMat() * transMat();
}

Mat4 RigidBody::inTensInv() const
{
	Mat4 rotMatT = rotMat();
	rotMatT.transpose();
	return rotMat() * d_inTensInv * rotMatT;
}
